public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Nam", 10);
        Customer customer2 = new Customer(2, "Lan", 20);
        System.out.println("Khách hàng 1 là: " + customer1);
        System.out.println("Khách hàng 2 là: " + customer2);
        Invoice invoice1 = new Invoice(11, customer1, 10000);
        Invoice invoice2 = new Invoice(22, customer2, 15000);
        System.out.println("Hóa đơn 1 là: " + invoice1);
        System.out.println("Hóa đơn 2 là: " + invoice2);
        System.out.println("Hóa đơn 1 có giá sau giảm là: " + invoice1.getAmountAfterDiscount());
        System.out.println("Hóa đơn 2 có giá sau giảm là: " + invoice2.getAmountAfterDiscount());
    }
}
